# Summary

* [Liberation Collective Manual](README.md)
* [Introduction](introduction.md)
* [Making Decisions](decisions.md)
