# Introduction

To understand why Liberation Collective exists, one must understand its history and acknowledge certain facts about the state of the animal liberation movement.
Other animal groups have inherent problems in the way that they're structured, leaving activists disempowered and taken advantage of.
Liberation Collective organizes in a different way, applying what we've learned working with other groups to create an organization that empowers activists and truly benefits animal liberation.

We do this by adopting two core organizing principals: **nonhierarchy** and **prioritizing local activism**.
This is explained in more detail below.

## History

Liberation Collective was created in Summer 2017 by several chapters of a certain animal rights org.
An internal conflict within that org thoroughly divided the activists into two camps: people who wanted the org's steering team to remain, and people who wanted the steering team to step down.
The two sides could not agree, and so the people who opposed the steering team were forced to split away; something we were not prepared for.

The org is fairly centralized in a single US city.
Many resources were shared with the org, and after splitting off we didn't have much left.
We didn't have our own website, donations platform, conflict resolution team, campaigns team, or sense of identity outside of the org.
Our groups were not able to function.

Some people suggested that we mirror the org's structure, except with a different name.
However, we felt we could do better than that, and prevent this type of problem from happening in the future.
Thus, Liberation Collective was formed.

## Local Organizing Comes First

We realized that it was more important to be on the ground actually making change than it was "to feel like part of a broader movement".
So we closed our laptops and started to focus on our city *specifically*.

This is different from what you're used to if you've been involved with other animal orgs in the past.
We reject that umbrella structure.
Liberation Collective is not an umbrella; it is a loose network of independent groups.

Liberation Collective is more like an extra label you can put on yourself.
It's not the main thing.
Be **independent**.
Be creative.
Come up with a name that suits your city.

Here are some examples of groups that are Liberation Collective members:
* Liberation Philadelphia
* Liberation Warsaw
* Animal Liberation Bay Area
* Species Revolution


Every city is different.
There's no "one-size-fits-all" activism you can apply to your city, as some animal orgs promise they can do.
Think critically about the environment you're in.
Has there been a history of activism in your city?
What are the demographics?
How vegan-friendly is your city?
The answers to these questions may affect the tactics you decide to use.
Be wary of organizations that lure you in with prescription activism.

Liberation Collective will *only do things that cannot be done at a local level*.
For example, organizing a convergence between groups in a similar region.
As another example, Liberation Collective wouldn't try to take donations directly, we would help each group set up their own donations platform and then direct visitors of our website to donate directly to their local group.

### Limited Public Presence

Liberation Collective doesn't need to have a public "personality".
If anything, the public doesn't need to know it exists.
We don't need to hide it either, but the more focus we put on the collective as a whole the more attention we take away from local groups.
This is the exact opposite of the purpose of Liberation Collective.
Liberation Collective's purpose is not to build an empire of animal liberation groups, it's to encourage animal liberation groups to be self-sustaining and join one-another in solidarity.
If we do have social media accounts, they should only aggregate content from other groups together, preferably in an automated way.
Otherwise this will create conflicts between the groups about which groups are getting the most attention.

## Nonhierarchy

Activism is voluntary.
Some of us feel like it's an imperative; that we have to do it because it's the right thing to do.
That may be true, but at the very least we should get to have some say in how it's done.
Activists are the ones doing the hard work, after all.

Members of our collective organize their groups as democracies.
Nobody is the boss of anyone else, and every member has exactly 1 vote.
Steps should be taken to formalize this process, such as having bylaws that all members have access to (you may see [Liberation Philly's bylaws](https://bylaws.liberationphilly.org/) as an example, which you're free to copy).
No single person runs the organization; the document does, and anyone can propose a change to the document.

Some groups use consensus (100% must agree in a vote), some use 2/3 majority, and basic majority is fine, too.
It's up to your group to decide what system works best for you, although we encourage you to make sure that everyone's voice is heard.
