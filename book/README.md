# Liberation Collective Manual

Liberation collective is an international [syndicate](https://peertube.social/videos/watch/cedbad95-a7bf-4ebb-8b8c-991da3fae4c2) of autonomous animal rights groups that share a set of values and generally use similar tactics.
Please start with the [Introduction](introduction.md) for an explanation for how this works.

The manual may be changed through the decision making process outlined in [Making Decisions](decisions.md).

## Contents

* [Introduction](introduction.md)
* [Making Decisions](decisions.md)
