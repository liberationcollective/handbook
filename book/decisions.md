# Making Decisions

This document describes how decisions are made that affect *the collective*, not individual groups.
Please read the [introduction](introduction.md) for more information.

**Decisions are made by forming consensus among voters**, with some additional provisions defined below.

## About Forming Consensus

> **Consensus decision making is a creative and dynamic way of reaching agreement between all members of a group.
Instead of simply voting for an item and having the majority of the group getting their way, a group using consensus is committed to finding solutions that everyone actively supports, or at least can live with.**
This ensures that all opinions, ideas and concerns are taken into account.
Through listening closely to each other, the group aims to come up with proposals that work for everyone.

> By definition, in consensus no decision is made against the will of an individual or a minority. If significant concerns remain unresolved, a proposal can be blocked and prevented from going ahead.
This means that the whole group has to work hard at finding solutions that address everyone's concerns rather than ignoring or overruling minority opinions.

> —[Seeds For Change](https://www.seedsforchange.org.uk/consensus)

For more information about how consensus decision-making works, please read [Shared Path, Shared Goal](https://ia802609.us.archive.org/7/items/SharedPathSharedGoal/shared_path_shared_goal-SCREEN.pdf).

## Scope

Because each group is independent, the scope of Liberation Collective is limited.
Liberation Collective will only do things that are otherwise impossible to do on a local level, such as organize convergences or maintain chatrooms and websites shared by the network.
Therefore, decisions concerning local organizing should not be made through the collective.

### Threshold of Relevancy

Consensus is only required from people who are directly affected by a decision, or from all voters when it affects either 1) the safety of the activists or 2) the strategy of the organization on a large scale.

If the threshold of relevancy is not reached, consensus from that specific individual is not required.
However, the input of all members into any decision, relevant or not, should always be taken very seriously.

### Abidance to Values

Proposals must not violate the group's [values](http://liberation.network/), unless it is a proposal to alter the group's values.

## Voting

Voting is done electronically through [a poll link](https://poll.disroot.org/) shared in the group's Riot chatroom.
Voters have the ability to agree, abstain, or disagree to the proposal. If no one disagrees, the proposal is passed.

If any person votes "disagree", they must state why, and then the group must discuss this issue until it is resolved, modifying the proposal if necessary.

### Eligibility

Any person who has attended 1 or more events held by a group that is a member of Liberation Collective within the past 6 months is eligible to vote.
Additionally, any person who has attended 1 or more events held by Liberation Collective itself (such as a convergence) within the past 6 months is eligible to vote.

### Blocking

It is also possible to **block** a proposal. Blocking a proposal means that all work on the proposal stops immediately. Voters may block by saying so in the Riot chatroom.

Blocking may be used if a proposal contains a critical flaw that cannot be fixed by simply revising the proposal.

Blocking a proposal is very serious, and if you block a proposal you are expected to thoroughly explain your position and then write a new proposal, within a reasonable time-frame, that addresses the issue at hand. Failure to put forth the necessary effort to do this may cause the group to proceed with the original proposal.

### Quorum

For forming consensus on a matter which requires the entire group, 25% of the group must participate in the process (rounded down to the nearest whole person).
For the sake of this metric, and this metric only, the "entire group" will be defined as the people in the Riot chatroom.

## Making a Proposal

A proposal is a formal statement suggesting that something be done.
Proposals should contain:
* Background information about the problem we are trying to solve.
* The different options we could hypothetically choose from to address the problem.
* A rationale for why one option is better than the others.

Proposals should be shared in the group's Riot chatroom, alongside a poll so people can vote on the proposal.
The proposal may be uploaded as a PDF document, pasted directly into the Riot chatroom, or written in the description of the poll itself.

Anyone can write and share a proposal at any time.

## Exceptions

In the case of a time-sensitive or unsafe situation, members are permitted to make one-time executive decisions.
Any member may do this when appropriate, however the following considerations should be taken into account:

1. Does this individual have the necessary skills, experience, and knowledge to be making this decision in this moment?

2. Is there any way to make this decision more open, even if it involves bringing in only a small number of people for now?

3. After an executive decision is made, what steps will be done to be transparent with the group and rectify any problems that may arise due to this decision?

Executive decisions should be only made in the rarest cases.
